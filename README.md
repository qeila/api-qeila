# yurtrip

## About Yurtrip

Yurtrip akan menjadi sebuah platform dimana user akan menggunakannya sebagai kebutuhan untuk berlibur. Apa saja yang di dapatkan dari Yurtrip?

- Destinasi Liburan yang up to date
- Menjamin semua transaksi aman dan terkendali
- Kemudahan dalam mencari teman berlibur
- Bagian dari rencana untuk liburan seperti Mencari Tour Guide, Menyewa Sarana untuk berlibur(Do Rent)
- Menabung untuk berlibur. Kalian bisa merencanakan waktu untuk berlibur, kemudian atur keuangan sehingga liburanmu menjadi mudah.