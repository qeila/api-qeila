<?php
namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;


class AuthenticationController extends Controller 
{

    public $successStatus = 200;

    private $response = [
        'status' => '',
        'code' => '',
        'title' => '',
        'message' => ''

    ];

    public function login(Request $request){

        if(Auth::attempt([
            'email' => request('email'), 
            'password' => request('password')
            ])){
                $user = Auth::user();
                $message = [
                    'title' => 'Login Berhasil',
                    'desc'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde eius excepturi eaque earum sit quod, illo officiis est, accusamus a eligendi? Saepe pariatur dolor recusandae incidunt cumque consectetur dolorem nemo.',
                ]; 
                $data =[
                    'message' => $message,
                    'token'   => $user->createToken('Yurtrip')-> accessToken,
                ]; 
                return response()->json(['success' => $data], $this-> successStatus); 
        }else{ 
            //Checking User Email exisiting or Not
            $email_checking = User::where('email',$request->email)->first();
            if (!$email_checking) {
                return response()->json(
                    $this-> response =[
                        'status'   => '401',
                        'code'     => 'member.not.registered',
                        'title'    => 'Anda belum terdaftar',
                        'message'  => 'Anda Belum Pernah Mendaftar Di System Kami'
                    ],401
                ); 
            } else {
                return response()->json(
                    $this-> response =[
                        'status'   => '400',
                        'code'     => 'credential.not.correct',
                        'title'    => 'Email atau Password salah',
                        'message'  => 'Pastikan Anda Menginput Data yang Benar'
                    ],400
                ); 
            }
        } 
    }


    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input              = $request->all(); 
        $input['password']  = bcrypt($input['password']); 
        $user               = User::create($input);
        $message            = [
                                'title' => 'Register Berhasil',
                                'Desc'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
                            ];
        $data               = [
                                'message'       => $message,
                                'Nama Anda'     => $user->name,
                                'Email Anda'    => $user->email,
                            ];
        $token['token']  =  $user->createToken('Yurtrip')-> accessToken;
            return response()->json(['success'=>$data], $this-> successStatus); 
    }


    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
}