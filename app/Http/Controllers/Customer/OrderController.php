<?php

namespace App\Http\Controllers\Member;

use App\Models\Member\Order; 
use App\Models\Member\Product;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

class OrderController extends Controller
{
    public $successStatus = 200;

    private $response = [
        'status' => '',
        'code' => '',
        'title' => '',
        'message' => ''

    ];

    public function daftarPesananKonsumen()
    {

        $order = Order::where('id_user', Auth::user()->id)->get();
        if ($booking) {
            $data = $order->toArray();

            $response = [
                'success' => true,
                'data' => $data,
                'message' => 'Booking Ditemukan'
            ];
            return response()->json($response, 200);
        } 

        $data = $order->toArray();
            $response = [
                'success' => true,
                'data' => $data,
                'message' => 'Pesanan Tidak Ditemukan'
            ];
    
        return response()->json($response, 400);

    }


    public function pesanProduk(Request $request, $id)
    {

        $validator = Validator::make($request->all(),[
            'id_product'   =>'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }
        //Find Id Product
        $product = Product::where('id',$request->id_product)->first();

        //Calculate total Harga Booking
    //    $total = sum($product->normal_proce);
    //    dd($total);
        
       
        //Make Booking 
        $order = new Order([
            'id_product'        => $product->id,
            'id_user'           => Auth::user()->id,
            'total'             => $request->total,
            'status'            => $request->status,
            'qty'               => $request->qty,
            'updated_at'        => null
        ]);
    
        $booking->save();
        
        $message =[
            'title' => 'Terima Kasih',
            'desc'  => 'Pesanan Anda Telah Kami Terima. Mohon Mengikuti Petunjuk Untuk Pembayaran'
        ];
        $data = [
            'products'  => $booking,
            'message'   => $message
        ];
        return response()->json(['success' => $data], $this-> successStatus);
    }
}
