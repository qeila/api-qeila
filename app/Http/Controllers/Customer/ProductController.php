<?php

namespace App\Http\Controllers\Member;

use App\Models\Member\Product; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

class ProductController extends Controller
{
    public $successStatus = 200;
   
    public function listproductall()
    {
        $product = Product::all();
        $data = $product->toArray();

        return response()->json(['success' => $data], $this-> successStatus);
    }

   
    public function listproductbymitra($id_mitra)
    {
        $product = Product::where('id_mitra',$id_mitra)->get();
        return response()->json(['success' => $product], $this-> successStatus);
    }

  
    public function listproductbyschedule(Request $request)
    {
        $product = Product::where('schedule',$request->schedule)->first;
        return response()->json(['success' => $product], $this-> successStatus);
    }
}
