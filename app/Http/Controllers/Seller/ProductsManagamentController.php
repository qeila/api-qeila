<?php

namespace App\Http\Controllers\Seller;


use App\Models\User; 
use App\Models\Seller\Categories;
use App\Models\Seller\Products;
use App\Models\Seller\ProductImages;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Validator;


class ProductsManagamentController extends Controller
{

    public $successStatus = 200;
    
    public function index()
    {
        $products = Products::where('id_seller', Auth::user()->id)->get();
        $data = $products->toArray();

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Product Ditemukan'
        ];

        return response()->json($response, 200);
    }

    public function indexKategori()
    {
        $categories = Categories::all();
        $data = $categories->toArray();
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Categories Ditemukan'
        ];

        return response()->json($response, 200);

    }

    public function storeKategori(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'categoryName'       => 'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }
        $categories = new Categories();
        $categories->categoryName = $request->input('categoryName');
        $categories->categoryParentsID = $request->input('categoryParentsID');
        $categories->is_parents = $request->input('is_parents');
        $categories->updated_at = null;
        $categories->save();
        
        $data = [
            'success'     => true,
            'data'  => $categories,
        ];
        return response()->json(['success' => $data], $this-> successStatus);
    }

    public function updateKategori(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'categoryName'       => 'required',
        ]);


        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        $categories = Categories::findOrFail($request->id)
        ->update([
            'categoryName'      => $request->categoryName,
            'categoryParentsID' => $request->categoryParentsID,
            'is_parents'        => $request->is_parents,
        ]);
        
        
        $data = [
            'success'  => true,
            'message'   => 'Successfully Updated!',
        ];
        return response()->json(['success' => $data], $this-> successStatus);
    }

    public function deleteKategori($id)
    {
        $categories = Categories::find($id);
        $categories->delete();

        return response()->json(['success' => $categories], $this-> successStatus);
    }


    public function createProducts(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'productName'       => 'required',
            'productPrice'      => 'required',
            'productWeight'     => 'required',
            'productShortDesc'  => 'required',
            'productLongDesc'   => 'required',
            'productCategoryID' => 'required',
            'productStock'      => 'required',
            'productSKU'        => 'required',
        ]);


        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }
    
        $products = new Products([
            'productName'       => $request->productName,
            'productPrice'      => $request->productPrice,
            'productWeight'     => $request->productWeight,
            'productShortDesc'  => $request->productShortDesc,
            'productLongDesc'   => $request->productLongDesc,
            'productCategoryID' => $request->productCategoryID,
            'productStock'      => $request->productStock,
            'productSKU'        => $request->productSKU,
            'updated_at'        => null
        ]);

        $products->save();
        
        $data = [
            'success'   => true,
            'data'  => $products,
        ];
        return response()->json(['result' => $data], $this-> successStatus);
    }
 
    public function show($id)
    {
        //
    }


    public function updateProducts(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'          =>'required',
            'descriptions'  =>'required',
            'include'       =>'required',
            'exclude'       =>'required',
            'meeting_points'=>'required',
            'terms'         =>'required',
            'normal_price'  =>'required',
            'schedule'      =>'required',
        ]);


        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        $products = Products::findOrFail($request->id)
        ->update([
            'name'              => $request->name,
            'descriptions'      => $request->descriptions,
            'include'           => $request->include,
            'exclude'           => $request->exclude,
            'notes'             => $request->notes,
            'meeting_points'    => $request->meeting_points,
            'terms'             => $request->terms,
            'normal_price'      => $request->normal_price,
            'sell_price'        => $request->sell_price,
            'exclude'           => $request->exclude,
            'discount_persen'   => $request->discount_persen,
            'schedule'          => $request->schedule,
        ]);
        
        $data = [
            'success'  => true,
            'data'   => $products
        ];
        return response()->json(['result ' => $data], $this-> successStatus);
    }

    public function uploadImageProducts(Request $request){

        $validator = Validator::make($request->all(),[
            'product_id'       => 'required',
            'images'      => 'required',
            'images_caption'     => 'required',
            'image_position'  => 'required',
            'is_cover'   => 'required',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        if($request->hasfile('images'))
         {
            foreach($request->file('images') as $images)
            {
                $name = time().'.'.$images->extension();
                $images->move(public_path().'/files/', $name);  
                $data[] = $name;  
            }
         }
         
        $images = new ProductImages([
            'images'            => json_encode($data),
            'product_id'        => $request->product_id,
            'images_caption'    => $request->images_caption,
            'image_position'    => $request->image_position,
            'is_cover'          => $request->is_cover,
        ]);

        $images->save();

        return back()->with('success', 'Data Your files has been successfully added');
    }

 
    public function deleteProducts($id)
    {
        $products = Products::find($id);
        $products->delete();

        return response()->json(['success' => $products], $this-> successStatus);
    }
}
