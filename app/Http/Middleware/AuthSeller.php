<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthSeller extends Middleware
{
   
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->is_seller != 1) {
            return response()->json(['message' => 'Anda Bukan Penjual'], 401);
        }

        return $next($request);
    }
}
