<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthSellers extends Middleware
{
    
    public function handle($request, Closure $next)
    {
        if (Auth::user()->is_seller != 1) {
            return response()->json(['message' => 'Anda Bukan Penjual'], 401);
        }
    
        return $next($request);
    }
}
