<?php

namespace App\Models\Member;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasApiTokens,Notifiable;

    protected $table = 'booking';
    protected $fillable = [
        'id_product',
        'id_user',
        'status',
        'total',
        'qty'
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\User');
    }
    
}
