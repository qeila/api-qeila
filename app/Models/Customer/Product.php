<?php

namespace App\Models\Member;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasApiTokens,Notifiable;

    protected $table = 'products';

    public function booking()
    {
        return $this->belongsTo('App\Models\Product');
    }
    
}
