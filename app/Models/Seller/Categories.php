<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;


class Categories extends Model
{
    use SoftDeletes;

    protected $table = 'categories'; 
    
    protected $fillable = [
        'categoryName', 
        'categorySlug',  
        'categoryParentsID',
        'is_parents',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function setcategoryNameAttribute($value)
    {
        $this->attributes['categoryName'] = $value;
        $this->attributes['categorySlug'] = Str::slug($value);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Seller\Products');
    }

}
