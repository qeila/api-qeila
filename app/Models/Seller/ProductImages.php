<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;


class ProductImages extends Model
{
    use SoftDeletes;

    protected $table = 'product_images';
    
    protected $fillable = [
        'product_id',
        'images',
        'images_caption',
        'is_cover',
        'image_position',
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function products()
    {
        return $this->hasOne('App\Models\Seller\Products');
    }

}
