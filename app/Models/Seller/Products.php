<?php

namespace App\Models\Seller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;


class Products extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    
    protected $fillable = [
        'productName',
        'productSlug',
        'productPrice',
        'productWeight',
        'productShortDesc',
        'productLongDesc',
        'productImage',
        'productImageMobile',
        'productCategoryID',
        'productStock',
        'productSKU',
        'updated_at'
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function setproductNameAttribute($value)
    {
        $this->attributes['productName'] = $value;
        $this->attributes['productSlug'] = Str::slug($value);
    }

    public function productImages()
    {
        return $this->hasMany('App\Models\Seller\ProductImages');
    }

    public function products()
    {
        return $this->hasOne('App\Models\Seller\Categories');
    }

}
