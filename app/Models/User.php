<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password','is_seller'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

   
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function booking()
    {
        return $this->hasMany('App\Models\Booking');
    }

    public function products(){
        if ($this->attributes['is_mitra'] == 1) {
            return $this->hasMany('App\Models\Mitra\Products');
        }
    }
}
