<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_mitra')->unsigned();
            $table->string('name');
            $table->longText('descriptions');
            $table->longText('include');
            $table->longText('exclude');
            $table->longText('notes');
            $table->longText('meeting_points');
            $table->longText('terms');
            $table->string('normal_price');
            $table->string('sell_price');
            $table->string('discount_persen');
            $table->date('schedule');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
