
<?php
// // Routes For Mitra B2B Start From Here //

// /* Route Auth Regster And Login As User */
// Route::post('loginb2b', 'Mitra\AuthenticationsController@login');
Route::post('registerb2b', 'Seller\AuthenticationsController@register');


// /* Route Groups With Middleware */

Route::group(['middleware' => ['auth:api']],function(){
    //Categories
    Route::get('kategori/all', 'Seller\ProductsManagamentController@indexKategori');
    Route::post('kategori/store', 'Seller\ProductsManagamentController@storeKategori');
    Route::post('kategori/update', 'Seller\ProductsManagamentController@updateKategori');
    Route::post('kategori/{id}/delete', 'Seller\ProductsManagamentController@deleteKategori');

    //Products
    Route::post('products/create', 'Seller\ProductsManagamentController@createProducts');
    Route::post('products/update', 'Seller\ProductsManagamentController@updateProducts');
    Route::get('products/{id}/delete', 'Seller\ProductsManagamentController@deleteProducts');

    //Uploa Image Product
    Route::post('products/uploadimages', 'Seller\ProductsManagamentController@uploadImageProducts');
});