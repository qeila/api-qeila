<?php

use Illuminate\Http\Request;

// Routes For Member Start From Here //

/* Route Auth Regster And Login As User */
Route::post('login', 'Customer\AuthenticationController@login')->name('login');
Route::post('register', 'Customer\AuthenticationController@register')->name('register');


// /* Route Groups product */
// Route::group(['middleware' => 'auth:api'],function(){
//     Route::get('product/all', 'Customer\ProductController@listproductall');
//     Route::get('product/{id_mitra}', 'Customer\ProductController@listproductbymitra');
//     Route::get('booked/{id_user}', 'Customer\BookingController@getuserbooked');
//     Route::post('booking/{id_product}', 'Customer\BookingController@bookorder');
// });

include('api-seller.php');
